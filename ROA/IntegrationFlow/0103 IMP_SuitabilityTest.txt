title IMP: Suitability Test

# Define participants
participantspacing equal

actor Customer
participant LU
participant RBW
lifelinecolor RBW #blue
lifelinecolor LU #orange
lifelinecolor Customer #gray
materialdesignicons F1B8 DB #206398


# Global Config
autoactivation 

box over Customer:Suitability Assessment
box over Customer,RBW:<align:center>OTP verification to receive **OTP Token**\n(Please see the OTP verification flow)</align>
Customer->LU:**Submit Suitability Test**\n-- - OTP Token--\n-- - Suitability Info--
box over LU:Calculate assessment scores
LU->RBW:**Submit Suitability Test**\n**-- - API Token --**\n**-- - OTP Token--**\n-- - Suitability Info--

box over RBW:<align:center>Suitability Test\nCalculate assessment scores\n</align>
RBW->*DB:**Save Suitability Test and score**
DB-->RBW:
destroysilent DB
RBW-->LU:**Response**\n-- - Assessment Score--


Customer<--LU:**Response**\n-- - Assessment Score--