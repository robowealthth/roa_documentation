title IMP: Get E-Wallet Transaction

# Define participants
participantspacing equal

actor Customer
participant LU
participant RBW
participant E-Wallet

lifelinecolor RBW #blue
lifelinecolor LU #orange
lifelinecolor E-Wallet #green
lifelinecolor Customer #gray

autoactivation 


Customer->LU:**Get E-Wallet Transaction**\n-- - startDate--\n-- - endDate--\n-- - pageNo--\n-- - pageSize--
LU->RBW:**Get E-Wallet Transaction**\n--** - API Token **--\n-- - CustomerID--\n-- - startDate--\n-- - endDate--\n-- - pageNo--\n-- - pageSize--
RBW->E-Wallet:**Get Balance**\n-- - X-Token--\n-- - reqRefNo--\n-- - acctNo--\n-- - startDate--\n-- - endDate--\n-- - statuses--\n-- - pageNo--\n-- - pageSize--
RBW<--E-Wallet:**Response**\n-- - respCode--\n-- - respDesc--\n-- - reqRefNo--\n-- - respRefNo--\n-- - items of transaction--
LU<--RBW:**Response**\n-- - transaction--
Customer<--LU:**Response**\n-- - transaction--
note over Customer,E-Wallet:**Transaction Parameters**                                                                                                                             \n-- - txDate--\n-- - txCode--\n-- - txName--\n-- - txDescription--\n-- - txAmount--\n-- - counterPartyName--\n-- - counterPartyMobileNo--\n-- - txDescription--\n-- - outstandingBalance--\n-- - availableBalance--\n-- - fee--\n-- - remark--\n-- - txStatusCode--\n-- - txStatusName--\n-- - trnNo--\n-- - currencyCode--\n-- - txnRequestRefNo--\n-- - refTrnNo--\n-- - approvalStatus--\n-- - clientTrnNo--\n-- - payeeCode--\n-- - payeeName--\n-- - refCode1--\n-- - refCode2--\n-- - refCode3--\n-- - refCode4--\n-- - tax--\n-- - channelCode--