## [0.0.2] - 2020-03-03
https://sequencediagram.org/

### Changed
* **0101 IMP_OpenAccount.txt**
  - Add calculation assessment score (processing box) on LU.
  - Add **API Token** param to all request from LU to RBW.
  - Separate suitability test section out and create new one.
* **0102 IMP_OpenAccount (Existing)**
  - Add **API Token** param to all request from LU to RBW.
  - Remove convert temporary account process
* **0201 IMP_TopUp.txt**
  - Update **parameter** for section that integrated with E-Wallet system
* **0202 IMP_Withdraw.txt**
  - Add **cut-off** section
  - Update **parameter** for all sections that integrated with E-Wallet system
* **0203 IMP_Get E-Wallet Balance**
  - Update **parameter** for section that integrated with E-Wallet system
* **0303 IMP_Dividend.txt**
  - Update **parameter** for all sections as Review
* **0301 IMP_Subscription.txt**
  - Update **parameter** for all sections that integrated with E-Wallet system.
    - Freeze cash
    - Transfer-buy process
* **0302 IMP_Redemption.txt**
  - Update **parameter** for all sections that integrated with E-Wallet system.
    - Transfer-sell process
* **0331_IMP_OpenAccount (Existing).txt**
  - Update for Existing customer verify mobile no.
### Added
- Add suitability test flow [0103 IMP_SuitabilityTest.txt]
- Add get account information flow [0104 IMP_GetAccountInformation.txt]
- Add Get E-Wallet Transaction flow [0204 IMP_Get E-Wallet Transaction.txt]


## [0.0.1] - 2020-02-27
- First draft version.